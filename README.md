# Twitter Followers List

App to watch followers of given user name

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

Clone with SSH
```
git@gitlab.com:kfir432/followers-list.git
```

Clone with https

```
https://gitlab.com/kfir432/followers-list.git
```
install node_modules in server folder - cd/server
```
yarn 
```
install node_modules in app
```
yarn 
```

## Running the app

cd/server to start the proxy server

```
yarn start
```

main directory run

```
yarn start
```

## Built With

* [React](https://reactjs.org/docs/getting-started.html) - The web framework used

## Authors

* **Kfir Evron** 

