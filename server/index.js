const express = require("express");
const cors = require("cors");
const app = express();
app.use(cors());

app.get("/twitter/followers/:screenId", (req, res) => {
  const request = require("request");
  const bearer =
    "AAAAAAAAAAAAAAAAAAAAAEIg%2BAAAAAAA8j9aSqTPnBHSiPhwslq0ROyZpCI%3DDAume2BrCDolZihBwabj2cofHeYZPxaVxBOJdS8ialZTrvn9m2";

  const screen_name = req.params.screenId;
  request(
    `https://api.twitter.com/1.1/followers/list.json?${screen_name}`,
    {
      auth: {
        bearer
      }
    },
    function(error, response, body) {
      if (error) console.log("error:", error);

      if (response) console.log("statusCode:", response && response.statusCode);

      if (body) res.status(200).send(body);
    }
  );
});

const port = process.env.PORT || 4001;

app.listen(port, () => {
  console.log(`Server is up on port ${port}!`);
});
