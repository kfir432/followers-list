import twitterApi from '../apis/twitterApi';
import {sortBy as _sortBy} from 'lodash';
import * as types from '../constants';

//fetchFollowers function
//params:
//screenName : String
//totalRows : Number
export const fetchFollowers = (screenName = '', totalRows = 30, cursor = -1) => async dispatch => {
    if (screenName.length > 0) {
        //show loading icon
        dispatch({type: types.IS_FETCHING, payload: true});
        const query = `screen_name=${screenName}&count=${totalRows}&cursor=${cursor}`;
        const response = await twitterApi.get(`/followers/${query}`);
        dispatch({type: types.FETCH_FOLLOWERS, payload: response.data});
        //hide loading icon
        dispatch({type: types.IS_FETCHING, payload: false});
        dispatch({type: types.INCREMENT, payload: false});
    } else {
        dispatch({type: types.REMOVE_FOLLOWERS, payload: []});
    }

};

export const searchTextChange = text => dispatch => {
    dispatch({type: types.SEARCH_TEXT, payload: text});
};

//sortFollowersBy function : sort by name param with lodash sortBy function
//params:
//name : String
export const sortFollowersBy = name => (dispatch, getState) => {
    let followers = getState().followers[0].users;
    let sortedFollowers = _sortBy(followers, (follower) => follower[name]);
    dispatch({type: types.SORT_FOLLOWERS, payload: sortedFollowers});
};
//fetchingData function : set up a loading icon when fetching data
//params:
//param : Boolean
export const fetchingData = param => dispatch => {
    dispatch({type: types.IS_FETCHING, payload: param});
};

export const increment = () => {
    return {
        type: types.INCREMENT
    }
};

export const decrement = () => {
    return {
        type: types.DECREMENT
    }
};