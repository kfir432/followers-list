import * as types from '../constants';

export default (state = {text:''}, action) => {
    if(action.type === types.SEARCH_TEXT){
        return {...state,text:action.payload};
    }
    return state;
}