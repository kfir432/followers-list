import * as types from '../constants';

export default (state = [], action) => {
    switch (action.type) {
        case types.FETCH_FOLLOWERS:
            if (action.payload.errors) {
                return [action.payload.errors];
            }
            return [...state, action.payload];
        case types.REMOVE_FOLLOWERS:
            return [];
        case types.SORT_FOLLOWERS:
            return state.map(item => {
                if (item.users.length > 0) {
                    return {
                        ...item,
                        users: action.payload
                    }
                }
                return item;
            });
        default:
            return state;
    }
}