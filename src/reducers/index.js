import {combineReducers} from 'redux';
import FollowersReducer from './FollowersReducer';
import IsFetchingReducer from './IsFetchingReducer';
import SearchReducer from './SearchReducer';

export default combineReducers({
    followers:FollowersReducer,
    isFetching:IsFetchingReducer,
    searchText:SearchReducer,
})