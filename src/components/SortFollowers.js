import React, {Component} from 'react';
import {connect} from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import Sort from '@material-ui/icons/Sort';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import * as actions from '../actions';
import {bindActionCreators} from 'redux';

const options = [
    {name: 'Sort By Name', value: 'name'},
    {name: 'Sort By Screen Name', value: 'screen_name'}
];

class SortFollowers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            anchorEl: null,
        };
    }

    handleClick = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };
    sortBy = value => {
        this.props.sortFollowersBy(value);
        this.handleClose();
    };

    render() {
        const {anchorEl} = this.state;
        const {followers} = this.props;
        const open = Boolean(anchorEl);
        const sortDisabled = Boolean(followers && followers[0] && followers[0].users &&
            followers[0].users.length > 0);
        return (
            <React.Fragment>
                <IconButton
                    aria-label="More"
                    aria-owns={open ? 'long-menu' : undefined}
                    aria-haspopup="true"
                    disabled={!sortDisabled}
                    onClick={this.handleClick}
                >
                    <Sort/>
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                >
                    {options.map(option => (
                        <MenuItem key={option.value} value={option.name} onClick={() => this.sortBy(option.value)}>
                            {option.name}
                        </MenuItem>
                    ))}
                </Menu>
            </React.Fragment>
        )

    }
}

const mapStateToProps = state => {
    return {
        followers: state.followers,
    }
};
const mapDispatchToProps = dispatch => {
    return bindActionCreators(actions, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(SortFollowers);