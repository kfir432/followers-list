import React, {Component} from 'react';
import {connect} from 'react-redux';
import FollowerItem from './FollowerItem';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import * as actions from '../actions';
import Loader from './Loader';
import {bindActionCreators} from 'redux';


class FollowersList extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll, true);
    }

    handleScroll = (e) => {
        const {fetchFollowers, searchText} = this.props;
        if (e.target.offsetHeight + e.target.scrollTop === e.target.scrollHeight) {
            window.clearTimeout( this.scrollTimeOut );
            let pageIndex = this.props.followers.length - 1;
            //if followers array is empty stop here
            if(this.props.followers.length <= 0){
                return
            }
            const cursorParamValue = this.props.followers[pageIndex].next_cursor;
            //prevent multiple request on scroll end with timeout
            this.scrollTimeOut = setTimeout(()=> {
                // Run the callback
                //Scrolling has stopped
                //bring next page
                fetchFollowers(searchText.text, 200, cursorParamValue);
            }, 50);
        }
    };

    renderList = () => {
        const {followers} = this.props;
        return followers.map((follower, index) => {
            if (follower.users)
                return follower.users.map(user => {
                    return (
                        <FollowerItem
                            key={user.id}
                            user={user}
                        />
                    )
                });
            //if no results or error
            return follower.map(message =>{
                return (
                    <ListItem alignItems="flex-start" key={`error-${message.code}`}>
                        <ListItemText
                            primary={message.message}
                        />
                    </ListItem>
                );

            });

        });
    };

    render() {
        const {isFetching,followers} = this.props;
        return (
            <React.Fragment>
                <List>
                    {followers.length > 0 ? this.renderList() : null}
                </List>
                {isFetching ? <Loader/> : null}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        followers: state.followers,
        isFetching: state.isFetching,
        searchText: state.searchText

    }
};
const mapDispatchToProps = dispatch => {
    return bindActionCreators(actions, dispatch)
};
export default connect(mapStateToProps, mapDispatchToProps)(FollowersList);