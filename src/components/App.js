import React, {Component} from 'react';
import FollowersList from './FollowersList';
import SearchInput from './SearchInput';
import SortFollowers from './SortFollowers';
import '../sass/style.css';

const App = () => {
    return (
        <div className="main-wrapper">
            <div className="main-list-wrapper">
                <div className="search-bar">
                    <SearchInput
                        label="Please Enter"
                        placeholder="Twitter Account Name"
                    />
                    <SortFollowers/>
                </div>
                <FollowersList/>
            </div>
        </div>);
};

export default App;