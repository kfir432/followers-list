import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

const Loader = () =>{
    return(
        <div className="loader-wrapper">
            <LinearProgress/>
        </div>
    )
};
export default Loader;