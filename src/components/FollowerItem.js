import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import React from "react";
import Typography from "@material-ui/core/Typography";

const FollowerItem = ({user}) =>{
  return(
      <ListItem alignItems="flex-start">
          <ListItemAvatar>
              <Avatar alt={user.name} title={user.name} src={user.profile_image_url}/>
          </ListItemAvatar>
          <ListItemText
              primary={user.description}
              secondary={
                  <React.Fragment>
                      <Typography component="span" color="textPrimary">
                          {user.screen_name}
                      </Typography>
                  </React.Fragment>
              }
          />
      </ListItem>
  )
};
export default FollowerItem;