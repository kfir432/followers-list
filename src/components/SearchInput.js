import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as actions from '../actions';
import {bindActionCreators} from 'redux';

class SearchInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            follower: ''
        }
    }

    handleChange = name => event => {
        let value = event.target.value;
        clearTimeout(this.timer);
        this.props.searchTextChange(value);
        //delay for request to avoid multiple requests to twitter api
        this.timer = setTimeout(() => {
            this.props.fetchFollowers(value);
        }, 500);


    };

    render() {
        const {placeholder, helperText, label,searchText} = this.props;
        return (
            <TextField
                className="search-follower"
                id="filled-full-width"
                label={label}
                style={{margin: 8}}
                placeholder={placeholder}
                helperText={helperText}
                value={searchText.text}
                fullWidth
                margin="normal"
                onChange={this.handleChange('follower')}
                variant="filled"
                InputLabelProps={{
                    shrink: true,
                }}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        searchFollower: state.searchFollower,
        searchText:state.searchText
    }
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(actions, dispatch)
};

SearchInput.propTypes = {
    placeholder: PropTypes.string,
    helperText: PropTypes.string,
    label: PropTypes.string,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchInput);